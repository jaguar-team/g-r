<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>GR</title>

        <?php wp_head(); ?>
    </head>
    
    <body>
        <header id="top">
            <div class="container">
                <div class="row">
                    <div class="logo col-lg-2 col-md-2 col-sm-3 col-xs-12">
                        <a href="<?= get_home_url(); ?>"><img src="<?= get_template_directory_uri().'/img/logo.png'; ?>"></a>
                    </div>
                    <div class="header-content col-lg-10 col-md-10 col-sm-9 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="etc">
                                    <a href="tel:<?= get_option('general_settings')['phone']; ?>"><span class="tel"><img src="<?= get_template_directory_uri().'/img/phone.png'; ?>"><?= get_option('general_settings')['phone']; ?></span></a>
                                    
                                    <?= wp_nav_menu(array('theme_location' => 'languages', 'container_class' => 'lang dropdown','container' => 'div')); ?>
                                    <span data-toggle="modal" data-target="#modal-login"><i class="fa fa-user"></i> <?php _e('Account', 'gr');?></span>
                                </div>
                            </div>
                            <div class="navigation col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                   
                                    <?= wp_nav_menu(array('theme_location' => 'top', 'container_class' => 'menu col-lg-9 col-md-10 hidden-sm hidden-xs','container' => 'nav')); ?>
                                    <nav class="collapsed-menu hidden-lg hidden-md col-sm-12 col-xs-12">

                                        <i class="menu-collapsed fa fa-bars visible-sm visible-xs dropdown-toggle" type="button" data-toggle="dropdown"></i>
                                        <button class="btn visible-sm visible-xs"  data-toggle="modal" data-target="#modal-investment"><?php _e('Invest', 'gr');?></button>
                                        <!--<ul class="dropdown-menu">
                                            <li><a href="#">О компании</a></li>
                                            <li><a href="#">Инвестиционные пакеты</a></li>
                                            <li><a href="#">Сотрудничество</a></li>
                                            <li><a href="#">FAQ</a></li>
                                            <li><a href="#">Контакты</a></li>
                                        </ul>-->
                                        <?= wp_nav_menu(array('theme_location' => 'top', 'menu_class' => 'dropdown-menu','container' => '')); ?>
                                    </nav>
                                    
                                    <div class="col-lg-3 col-md-2 col-sm-12 col-xs-12">
                                        <button class="btn hidden-sm hidden-xs" data-toggle="modal" data-target="#modal-investment"><?php _e('Invest', 'gr');?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>    