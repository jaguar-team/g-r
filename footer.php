<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage money-online
 */

?>
		 <footer>		 	
		 	<?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
		 	<div class="container">
		 		<div class="row footer-top">
		 			<div class="logo col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <a href="#"><img src="<?= get_template_directory_uri().'/img/logo.png'; ?>"></a>
                    </div>
                    <div class="content text-center-xs col-lg-9 col-md-9 col-sm-9 col-xs-12">
	                    <div class="row">
	                    	
	                    	<?= wp_nav_menu(array('theme_location' => 'footer_1', 'menu_class' => 'col-lg-4 col-md-4 col-sm-4 col-xs-12','container' => '')); ?>
	                    	
	                    	<?= wp_nav_menu(array('theme_location' => 'footer_2', 'menu_class' => 'col-lg-4 col-md-4 col-sm-4 col-xs-12','container' => '')); ?>
	                    	
	                    	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
	                    		<button class="btn" data-toggle="modal" data-target="#modal-investment"><?php _e('Инвестировать');?></button>
	                    		<span class="tel"><?= get_option( 'general_settings' )['phone']; ?></span>
	                    		<span>Пн-Пт 9.00 - 19.00</span>
	                    		<span class="link"><a href="<?= get_option( 'general_settings' )['email']; ?>"><?= get_option( 'general_settings' )['email']; ?></a></span>
	                    	</div>
	                    </div>
                    </div>
		 		</div>
		 		<div class="row footer-bottom">
		 			<div class="content text-center-xs">
			 			<ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                		<li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><a href="">© <?= date('Y'); ?> GregoryRoman</a></li>
	                		<li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><a href=""><?php _e('Юридическая информация');?></a></li>
	                		<li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><a href=""><?php _e('Политика конфиденциальности');?></a></li>
	                		<li class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><a href=""><?php _e('Предупреждение о рисках');?></a></li>
	                	</ul>
	                </div>
		 		</div>
		 	</div>
		 </footer>
		
	</body>
	<?php get_template_part('part/modal-windows'); ?>
</html>