jQuery(function($){

	function Menu(){
		var top = $(document).scrollTop();
		if (top < 800) {
			$("#toTop").removeClass('float');							
		}				
		else {
			$("#toTop").addClass('float');						
		}
	}

	$(document).ready(function(){

		$('#toTop').click(function(){
	        var el = $(this).attr('href');
	        $('body').animate({
	            scrollTop: $(el).offset().top}, 700);
	        return false; 
		});
		
		$('.advantage .block').mouseenter(function(){
			$(this).find('.desc.short').hide('.3s ease-in-out');
			$(this).find('.desc.full').show('.3s ease-out-in');
		}).mouseleave(function(){
			$(this).find('.desc.full').hide('.3s ease-in-out');
			$(this).find('.desc.short').show('.3s ease-out-in');
		});

		$('a[data-toggle="collapse"]').click(function(){
			if($(this).parent().find('i').hasClass('fa-angle-down')){
				$(this).parent().find('i').remove();
				$(this).parent().prepend('<i class="fa fa-angle-up"></i>');
			}
			else{
				$(this).parent().find('i').remove();
				$(this).parent().prepend('<i class="fa fa-angle-down"></i>');
			}
		});

		$('.lang > ul > li > a').addClass('dropdown-toggle').attr('data-toggle', 'dropdown').append(' <i class="fa fa-angle-down"></i>');
		$('.lang .sub-menu').addClass('dropdown-menu');
		

		$('.top-slider').slick({
			slidesToShow: 1,
		    slidesToScroll: 1,
		    autoplay: true,
		    autoplaySpeed: 4000,
		    dots: true,
		    infinite: true,
		    arrows: true,
		   	nextArrow: '<div class="slider-arrows next"></div>',
		   	prevArrow: '<div class="slider-arrows prev"></div>',
		    responsive: [
			    {
			      breakpoint: 1199,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			        infinite: true,
			      }
			    },		    
	    	]
		});
		
	});
});