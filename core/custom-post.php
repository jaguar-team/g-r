<?php
/*
 * Desc: Register a post type.
 * Name: investment-company
 */
function investment_company_init() {
    $labels = array(
        'name'                  => __( 'Investment companies', 'gr' ),
        'singular_name'         => __( 'Investment company', 'gr' ),
        'menu_name'             => __( 'Investment companies', 'gr' ),
        'name_admin_bar'        => __( 'Investment company', 'gr' ),
        'add_new'               => __( 'Add investment company', 'gr' ),
        'add_new_item'          => __( 'Add new investment company', 'gr' ),
        'new_item'              => __( 'New investment company', 'gr' ),
        'edit_item'             => __( 'Edit a investment company', 'gr' ),
        'all_items'             => __( 'All Investment companies', 'gr' ),
        'search_items'          => __( 'Find a investment company', 'gr' ),
        'not_found'             => __( 'Investment companies are not found', 'gr' ),
        'not_found_in_trash'    => __( 'Investment companies are not in a cart', 'gr' ),
    );

    $args = array(
        'labels'                => $labels,
        'public' 				=> true,
        'publicly_queryable' 	=> true,
        'show_ui'               => true,
        'has_archive' 		    => false,
        'menu_position'		    => 4,
        'menu_icon'             => 'dashicons-admin-multisite',
        'supports'              => array( 'title' ),
    );

    register_post_type( 'investment-company' , $args);
}
add_action( 'init', 'investment_company_init' );

/*
 * Add extra fields
 */
function investment_company_extra_fields() {
    add_meta_box(
        'extra_fields',
        __( 'Setting investment company' ),
        'investment_company_fields',
        'investment-company',
        'normal',
        'high'
    );
}
add_action( 'add_meta_boxes', 'investment_company_extra_fields', 1 );

/*
 * Customize default column
 */
function investment_company_custom_column($defaults) {

    $columns = array(
        'cb'	 		=> '<input type="checkbox" />',
        'title'			=> __( 'Name' , 'gr' ),
        'amount'		=> __( 'Amount', 'gr' ),
        'rate' 			=> __( 'Rate', 'gr' ).' <span class="dashicons dashicons-chart-bar"></span>',
        'period'        => __( 'Period', 'gr' ).' <span class="dashicons dashicons-clock"></span>',
        'date'			=> __( 'Date', 'gr' ) .'<span class="dashicons dashicons-calendar"></span>',
    );
    return $columns;
}
add_filter( 'manage_investment-company_posts_columns', 'investment_company_custom_column' );

/*
 * Add sortable fields
 */
function investment_company_sortable( $columns ) {
    $columns['rate'] = 'rate';
    return $columns;
}
add_filter( "manage_edit-investment-company_sortable_columns", "investment_company_sortable" );

/*
 * Get value to column fields
 */
function investment_company_get_column_value($column, $post_id) {
    if ( $column == 'amount' )
        if ( get_post_meta( $post_id, 'vip', 1 ) )
            echo  '<b style="color:#228b22;">'.number_format( get_post_meta( $post_id, 'amount-from', 1 ) , 2, ',', ' ').' - ... $';
        else
            echo  '<b style="color:#228b22;">'.number_format( get_post_meta( $post_id, 'amount-from', 1 ) , 2, ',', ' ').' - '.number_format( get_post_meta( $post_id, 'amount-to', 1 ) , 2, ',', ' ').' $.</b>';

    if ( $column == 'rate' )
        if ( get_post_meta( $post_id, 'vip', 1 ) )
            echo  __('Individually');
        else
            echo  '<b>'.get_post_meta( $post_id, 'interest-rate', 1 ).'</b>%.';

    if ( $column == 'period' )
        if ( get_post_meta( $post_id, 'vip', 1 ) )
            echo  __('Individually');
        else
            echo  '<b>'.get_post_meta( $post_id, 'interest-period', 1 ).'</b> '.__('month');
}
add_action( 'manage_investment-company_posts_custom_column', 'investment_company_get_column_value', 10, 2 );

/*
 * Extra fields
 */
function investment_company_fields( $post ){
    ?>

    <p>
        <label><?= __('The amount of investment', 'gr'); ?>: </label>
        <input type="text" placeholder="5000" name="investment_company[amount-from]" value="<?= get_post_meta( $post->ID, 'amount-from', 1 ) ?>">$ -
        <input type="text" placeholder="10000" name="investment_company[amount-to]" value="<?= get_post_meta( $post->ID, 'amount-to', 1 ) ?>">$
    </p>

    <p>
        <label><?= __('Interest rate', 'gr'); ?>: </label>
        <input type="text" placeholder="5" name="investment_company[interest-rate]" value="<?= get_post_meta( $post->ID, 'interest-rate', 1 ) ?>">%
    </p>

    <p>
        <label><?= __('Period of investment', 'gr'); ?>: </label>
        <input type="number" minlength="1" maxlength="48" placeholder="3" name="investment_company[interest-period]" value="<?= get_post_meta( $post->ID, 'interest-period', 1 ) ?>"> <?= __('month(s)', 'gr'); ?>
    </p>

    <p>
        <?php $mark_vip = get_post_meta($post->ID, 'vip', 1); ?>
        <label><?= __('VIP', 'gr'); ?>: <input type="radio" name="investment_company[vip]" value="0" <?php checked( $mark_vip, 0 ) ?> checked /><span class="dashicons dashicons-no"></span></label>
        <label><input type="radio" name="investment_company[vip]" value="1" <?php checked( $mark_vip, 1 ) ?> /><span class="dashicons dashicons-yes"></span></label>
    </p>

    <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

    <input name="save" type="submit" class="button button-primary button-large" value="Сохранить">

    <?php
}

/*
 * Save investment extra fields
 */
function investment_company_update($post_id ) {
    if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false;
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false;
    if ( !current_user_can('edit_post', $post_id) ) return false;

    if( !isset($_POST['investment_company']) ) return false;

    foreach( $_POST['investment_company'] as $key=>$value ){
        if( empty($value) ){
            delete_post_meta( $post_id, $key );
            continue;
        }

        update_post_meta($post_id, $key, $value);
    }
    return $post_id;
}
add_action( 'save_post', 'investment_company_update', 0 );

/*
 * Desc: Register a post type.
 * Name: slider
 */
function slider_init() {
    $labels = array(
        'name'                  => __( 'Home page slider', 'gr' ),
        'singular_name'         => __( 'Home page slider', 'gr' ),
        'menu_name'             => __( 'Slider', 'gr' ),
        'name_admin_bar'        => __( 'Slider', 'gr' ),
        'add_new'               => __( 'Add slide', 'gr' ),
        'add_new_item'          => __( 'Add new slide', 'gr' ),
        'new_item'              => __( 'New slide', 'gr' ),
        'edit_item'             => __( 'Edit slide', 'gr' ),
        'all_items'             => __( 'All Slides', 'gr' ),
        'search_items'          => __( 'Find a slides', 'gr' ),
        'not_found'             => __( 'Slides are not found', 'gr' ),
        'not_found_in_trash'    => __( 'Slides are not in a cart', 'gr' ),
    );

    $args = array(
        'labels'                => $labels,
        'public' 				=> true,
        'publicly_queryable' 	=> true,
        'show_ui'               => true,
        'has_archive' 		    => false,
        'menu_position'		    => 4,
        'menu_icon'             => 'dashicons-format-gallery',
        'supports'              => array( 'title', 'thumbnail', 'editor' ),
    );

    register_post_type( 'slider' , $args);
}
add_action( 'init', 'slider_init' );

/*
 * Customize default column
 */
function slider_custom_column($defaults) {

    $columns = array(
        'cb'	 		=> '<input type="checkbox" />',
        'img'           => '<span class="dashicons dashicons-format-image"></span>',
        'title'			=> __( 'Name' , 'gr' ),
        'desc'			=> __( 'Description' , 'gr' ),
        'date'			=> __( 'Date', 'gr' ) .'<span class="dashicons dashicons-calendar"></span>',
    );

    return $columns;
}
add_filter( 'manage_slider_posts_columns', 'slider_custom_column' );

/*
 * Get value to column fields
 */
function slider_get_column_value($column, $post_id) {
    if( $column == 'img' )
        echo get_the_post_thumbnail( $post_id, array( 60, 60 ) );

    if( $column == 'desc' )
        echo get_the_content( $post_id );
}
add_action( 'manage_slider_posts_custom_column', 'slider_get_column_value', 10, 2 );

/*
 * Desc: Register a post type.
 * Name: questions
 */
function questions_init() {
    $labels = array(
        'name'                  => __( 'Questions', 'gr' ),
        'singular_name'         => __( 'Questions', 'gr' ),
        'menu_name'             => __( 'Questions', 'gr' ),
        'name_admin_bar'        => __( 'Questions', 'gr' ),
        'add_new'               => __( 'Add question', 'gr' ),
        'add_new_item'          => __( 'Add new question', 'gr' ),
        'new_item'              => __( 'New question', 'gr' ),
        'edit_item'             => __( 'Edit question', 'gr' ),
        'all_items'             => __( 'All questions', 'gr' ),
        'search_items'          => __( 'Find a questions', 'gr' ),
        'not_found'             => __( 'Questions are not found', 'gr' ),
        'not_found_in_trash'    => __( 'Questions are not in a cart', 'gr' ),
    );

    $args = array(
        'labels'                => $labels,
        'public' 				=> true,
        'publicly_queryable' 	=> true,
        'show_ui'               => true,
        'has_archive' 		    => false,
        'menu_position'		    => 7,
        'menu_icon'             => 'dashicons-editor-help',
        'supports'              => array( 'title', 'thumbnail', 'editor' ),
    );

    register_post_type( 'questions' , $args);
}
add_action( 'init', 'questions_init' );

/*
 * Desc: Register a post type.
 * Name: advantages
 */
function advantages_init() {
    $labels = array(
        'name'                  => __( 'Advantages', 'gr' ),
        'singular_name'         => __( 'Advantages', 'gr' ),
        'menu_name'             => __( 'Advantages', 'gr' ),
        'name_admin_bar'        => __( 'Advantages', 'gr' ),
        'add_new'               => __( 'Add advantage', 'gr' ),
        'add_new_item'          => __( 'Add new advantage', 'gr' ),
        'new_item'              => __( 'New advantage', 'gr' ),
        'edit_item'             => __( 'Edit advantage', 'gr' ),
        'all_items'             => __( 'All advantages', 'gr' ),
        'search_items'          => __( 'Find a advantages', 'gr' ),
        'not_found'             => __( 'Advantages are not found', 'gr' ),
        'not_found_in_trash'    => __( 'Advantages are not in a cart', 'gr' ),
    );

    $args = array(
        'labels'                => $labels,
        'public' 				=> true,
        'publicly_queryable' 	=> true,
        'show_ui'               => true,
        'has_archive' 		    => false,
        'menu_position'		    => 3,
        'menu_icon'             => 'dashicons-pressthis',
        'supports'              => array( 'title', 'thumbnail', 'editor' ),
    );

    register_post_type( 'advantages' , $args);
}
add_action( 'init', 'advantages_init' );