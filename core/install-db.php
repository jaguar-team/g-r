<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 21.11.2016 13:13
 * Version: 1.0.0
 */

function install_db() {
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    global $wpdb;

    $table_name = $wpdb->prefix . "gr_users";
    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {

        $sql = 'CREATE TABLE `'.$table_name.'` (
            `id`                      mediumint(9) NOT NULL AUTO_INCREMENT,
            `email`                   VARCHAR(255) NOT NULL,
            `password`                VARCHAR(255) NOT NULL,
            `access_token`            VARCHAR(255) NOT NULL,
            `date_upd`                DATETIME NOT NULL,
            `date_add`                DATETIME NOT NULL,
            UNIQUE KEY id (id)
        );';
        dbDelta($sql);
    }

    $table_name = $wpdb->prefix . "gr_users_info";
    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {

        $sql = 'CREATE TABLE `'.$table_name.'` (
            `id`                      mediumint(9) NOT NULL AUTO_INCREMENT,
            `id_user`                 mediumint(9) NOT NULL,
            `first_name`              VARCHAR(255) NULL,
            `last_name`               VARCHAR(255) NULL,
            `phone_number`            VARCHAR(255) NULL,
            `country`                 VARCHAR(255) NULL,
            `city`                    VARCHAR(255) NULL,
            `address`                 VARCHAR(255) NULL,
            `date_upd`                DATETIME NOT NULL,
            `date_add`                DATETIME NOT NULL,
            UNIQUE KEY id (id),
            FOREIGN KEY (id_user) REFERENCES '.$wpdb->prefix.'gr_users'.' (id)
        );';
        dbDelta($sql);
    }

    $table_name = $wpdb->prefix . "gr_bids";
    if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {

        $sql = 'CREATE TABLE `'.$table_name.'` (
            `id`                      mediumint(9) NOT NULL AUTO_INCREMENT,
            `email`                   VARCHAR(255) NULL,
            `phone`                   VARCHAR(255) NULL,
            `full_name`               VARCHAR(255) NULL,
            `date_upd`                DATETIME NOT NULL,
            `date_add`                DATETIME NOT NULL,
            UNIQUE KEY id (id)
        );';
        dbDelta($sql);
    }
}
add_action('init', 'install_db');
// after_switch_theme