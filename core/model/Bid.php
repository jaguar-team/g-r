<?php

/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 24.11.2016 17:20
 * Version: 1.0.0
 */
class Bid extends ObjectModel {

    public $email;

    public $phone;

    public $full_name;

    public $data_upd;

    public $data_add;

    public static $definition = array(
        'table' 	=> 'gr_bids',
        'primary' 	=> 'id',
        'name'		=> 'bid_data',
        'fields' => array(
            'email' 		=> array('validate' => 'is_email'),
            'phone' 		=> array('validate' => 'is_string', 'required' => true),
            'full_name' 	=> array('validate' => 'is_string', 'required' => true),
            'date_upd'		=> array('validate' => 'is_string', 'required' => true),
            'date_add'		=> array('validate' => 'is_string', 'required' => true),
        ),
    );

    public function attributeLabels()
    {
        return array(
            'email' 	    =>  __('Email', 'gr'),
            'phone' 	    =>  __('Phone', 'gr'),
            'full_name' 	=>  __('Full name', 'gr'),
        );
    }

    public function rules()
    {
        $label = $this->attributeLabels();
        return array(
            'form'	=> array(
                'action' 			=> '',
                'method'			=> 'POST',
                'class'				=> 'form',
            ),
            'input'	=> array(
                'full_name' 	=>  array(
                    'before'        => '<div class="form-group row"><label for="name" class="col-lg-3">'.$label['full_name'].'*</label>',
                    'after'         => '</div>',
                    'type' 			=> 'text',
                    'class'         => 'col-lg-9',
                    'id'            => 'name',
                    'placeholder'   => 'Enter your full name'
                ),
                'phone' 	=>  array(
                    'before'        => '<div class="form-group row"><label for="tel" class="col-lg-3">'.$label['phone'].'*</label>',
                    'after'         => '</div>',
                    'type' 			=> 'text',
                    'class'         => 'col-lg-9',
                    'id'            => 'tel',
                ),
                'email' 	=>  array(
                    'before'        => '<div class="form-group row"><label for="email" class="col-lg-3">'.$label['email'].'</label>',
                    'after'         => '</div>',
                    'type' 			=> 'email',
                    'class'         => 'col-lg-9',
                    'id'            => 'email',
                ),
            ),
            'submit' => array(
                'before'			=> '<div class="row">',
                'after'				=> '</div>',
                'value'             => __('Send', 'gr'),
                'class' 			=> 'col-lg-9 col-lg-offset-3',
            ),
        );
    }

    public function add()
    {
        parent::__construct();
    }
}