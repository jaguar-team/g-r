<?php
/**
 * Author: Vlad Dneprov, https://www.linkedin.com/in/vladislav-dneprov
 * Author URI: http://jaguar-team.com
 * Date: 11.11.2016
 * Version: 1.0.0
 */

class ObjectModel {

	/**
	 * @var
     */
	public $id;

	/**
	 * @var
     */
	protected $data_fields;

	/**
	 * @var
     */
	protected $def;

	/**
	 * @var
     */
	protected $class;

	/**
	 * @var
     */
	protected $class_name;

	/**
	 * @var
     */
	protected $table;

	/**
	 * @var
     */
	protected $name;

	/**
	 * @var
     */
	protected $identifier;

	/**
	 * @var
     */
	protected $wpdb;

	/**
	 * @var
     */
	protected $type;

	/**
	 * @var
     */
	protected $rules;

	/**
	 * @var
     */
	protected $attribute_labels;

	/**
	 * @var
     */
	protected static $reflection;

	/**
	 * @var
     */
	public static $post_name;

	/**
	 * @var array
     */
	public $message_errors = array();

	/**
	 * ObjectModel constructor.
     */
	public function __construct()
	{
		$this->init();
	}

	/**
	 * @return $this
	 * @throws Exception
     */
	public function init()
	{
		if (empty($this->class) && get_class($this) != $this->class) {
			global $wpdb;
			$this->class = $this;
			$this->class_name = get_class($this);
			$class_name = get_class($this);

			$this->def = ObjectModel::getDefination($class_name);
			if ($this->def !== false) {
				$this->wpdb = $wpdb;
				$this->setDefinitionRetrocompatibility();
				if($this->setFields()) {
					$this->validateFields();
				}
			}
			else {
				throw new Exception('Class '.$class_name.' not have required properties.');
			}
		}

		return $this;
	}

	/**
	 * @return bool
     */
	public function validateFields()
	{
		foreach ( $this->data_fields as $field => $value ) {
			$message = $this->validateField($field, $this->class->$field);

			if ($message !== true)
				$this->message_errors[$field] = $message;
		}

		return true;
	}

	/**
	 * @param $field
	 * @param $value
	 * @return bool|string
     */
	public function validateField($field, $value)
	{
		$human_field = (isset($this->attribute_labels[$field]) ? $this->attribute_labels[$field] : $field);

		$data = $this->def['fields'][$field];
		// Check required
		if (isset($data['required'])){
			if ($data['required'])
				if (empty($value))
					return sprintf(__('Field %s is required.', 'gr'), $human_field);
		}

		// Check size 
		if (isset($data['size'])) {
			$size = $data['size'];
			$length = strlen($value);

			// Check min size
			if (isset($size['min']) && $length < $size['min'])
				return sprintf(__('Field %s must contain more %d symbols.', 'gr'), $human_field, $size['min']);
			// Check max size
			if (isset($size['max']) && $length > $size['max'])
				return sprintf(__('Field %s must contain more %d symbols.', 'gr'), $human_field, $size['max']);
		}

		// Check field validator
		if (isset( $data['validate']))
			if (!call_user_func( $data['validate'], $value ))
				return sprintf(__('Field %s not valid', 'gr'), $human_field);

		return true;
	}

	/**
	 * @param bool $auto_date
	 * @return bool
     */
	public function add($auto_date = true)
    {
    	if ($this->message_errors)
    		return false;

		if ($auto_date && property_exists($this->class, 'date_add'))
			$this->class->date_add = date('Y-m-d H:i:s');

		if ($auto_date && property_exists($this->class, 'date_upd'))
			$this->class->date_upd = date('Y-m-d H:i:s');


		$data = array();
		foreach ($this->def['fields'] as $field => $value) {
			$data[$field] = $this->class->{$field};
		}
		
		if (!$this->wpdb->insert($this->table, $data))
			return false;

		$this->id = $this->wpdb->insert_id;

    	return true;
    }

	/**
	 * @return bool
     */
	public function update()
	{
		if ($this->message_errors)
			return false;

		if (property_exists($this, 'date_upd'))
			$this->data_fields['data_upd'] = date('Y-m-d H:i:s');

		/**$format = array();
		foreach ($this->data_fields as $field => $data) {
			$format[] = $this->type[$field];
		}**/

		$where = array($this->def['primary'] => $this->id);

		if($this->wpdb->update($this->table, $this->data_fields, $where) === false)
			return false;

		return true;
	}

	/**
	 * @return bool
     */
	public function delete()
	{
		return true;
	}

	/**
	 * @return bool
     */
	public function save()
	{
		return ($this->id ? $this->update() : $this->add());
	}

	/**
	 * @param string $method
	 * @param bool $action
	 * @return string
	 * @throws Exception
     */
	public function getForm($method = 'POST', $action = true)
    {
    	if (empty($this->rules))
    		throw new Exception('Method rules() of class  not be return empty property.');

    	$rules = $this->rules();

    	/** generation form **/
    	$form = '<form ';
    	foreach ($rules['form'] as $name => $value) 
    		$form .= $name.'="'.$value.'" ';
    	$form .= '>';

		/** if isset id */
		$form .= (!empty($this->id) ? '<input name="'.$this->def['name'].'[id]" type="hidden" value="'.$this->id.'" />' : '');

    	/** generation input **/
    	foreach ($rules['input'] as $name => $values) {

			if (isset($values['class']))
				$class = $values['class'].' ';
			if (isset($this->message_errors[$name]))
				if (isset($class))
					$class .= 'invalid';
				else
					$class = 'invalid';

    		$form .= (isset($values['before']) ? $values['before'] : "");
    		$form .=
    			'<input 
    				name="'.$this->def['name'].'['.$name.']" '
    				.(isset($values['type']) ? 'type="'.$values['type'].'" ' : "")
    				.(isset($class) ? 'class="'.$class.'" ' : "")
    				.(isset($values['id']) ? 'id="'.$values['id'].'" ' : "")
					.(isset($values['placeholder']) ? 'placeholder="'.$values['placeholder'].'" ' : "")
					.(property_exists($this, $name) ? 'value="'.$this->class->{$name}.'" ' : "")
    				.(isset($values['required']) ? 'required' : "").
    			' />';
			$form .= (isset($this->message_errors[$name]) ? '<p>'.$this->message_errors[$name].'</p>' : '');
    		$form .= (isset($values['after']) ? $values['after'] : "");
    	}

    	/** generation input submit **/
    	$form .= (isset($rules['submit']['before']) ? $rules['submit']['before'] : "");
    	$form .= '
    			<input 
    				type="submit" name="'.$this->class_name.'" '
    				.(isset($rules['submit']['id']) ? 'id="'.$rules['submit']['id'].'" ' : "")
    				.(isset($rules['submit']['class']) ? 'class="'.$rules['submit']['class'].'" ' : "")
					.(isset($rules['submit']['value']) ? 'value="'.$rules['submit']['value'].'" ' : "").
    			 ' />';
    	$form .= (isset($rules['submit']['after']) ? $rules['submit']['after'] : "");
    	
    	$form .= '</form>';
    	return $form;
    }

	/**
	 * @param $class
	 * @return bool|mixed
     */
	public static function getDefination($class)
	{
		if (is_object($class))
            $class = get_class($class);

        self::$reflection = new ReflectionClass($class);


        if (!self::$reflection->hasProperty('definition'))
        	return false;

        $definition = self::$reflection->getStaticPropertyValue('definition');


        if (!$definition || !isset($definition['table'])
        				 || !isset($definition['name'])
        				 || empty($definition['table'])
        				 || empty($definition['name']))
        	return false;

        $definition['classname'] = $class;

        return $definition;
	}

	/**
	 * @param $array_object
	 * @return array|bool
     */
	public function transformToObjectClass($array_object)
	{
		if (!is_array($array_object) || !$array_object || empty($array_object) || !$this->class_name)
			return false;

		$results = array();
		foreach ($array_object as $number => $array) {
			$object = new $this->class_name();

			foreach ($array as $name => $value) {
				if (property_exists($object, $name))
					$object->{$name} = $value;
			}
			$results[] = $object;
		}

		return $results;
	}

	/**
	 * @return bool
     */
	protected function setFields()
	{
		if (self::$reflection->hasMethod('attributeLabels'))
			$this->attribute_labels = $this->class->attributeLabels();

		if (self::$reflection->hasMethod('rules'))
			$this->rules = $this->class->rules();

		if (!isset($_POST[$this->def['name']]))
			return false;

		$this->data_fields = $_POST[$this->def['name']];

		foreach ($this->data_fields as $field => $value) {
			$this->class->{$field} = $value;
		}

		return true;
	}

	/**
	 * @return $this
     */
	protected function setDefinitionRetrocompatibility()
    {
        // Retrocompatibility with $table property ($definition['table'])
        if (isset($this->def['classname'])) {
            $this->classname = $this->def['classname'];
        }

        if (isset($this->def['table'])) {
            $this->table = $this->wpdb->prefix.$this->def['table'];
        } else {
            $this->def['table'] = $this->table;
        }

        // Retrocompatibility with $identifier property ($definition['primary'])
        if (isset($this->def['primary'])) {
            $this->identifier = $this->def['primary'];
        } else {
            $this->def['primary'] = $this->identifier;
        }

        if (isset($this->def['name'])) {
            $this->name = $this->def['name'];
        } else {
            $this->def['primary'] = $this->name;
        }

        foreach ($this->def['fields'] as $field => $value)
        	$this->type[$field] = (isset($field['type']) ? $field['type'] : '%s');

        return $this;
    }
}