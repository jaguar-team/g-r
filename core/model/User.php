<?php

/**
 * Class User
 */
class User extends ObjectModel {

	/**
	 * @var
     */
	public $email;

	/**
	 * @var
     */
	public $password;

	/**
	 * @var
     */
	public $access_token;

	/**
	 * @var
     */
	public $date_upd;

	/**
	 * @var
     */
	public $date_add;

	/**
	 * @var array
     */
	public static $definition = array(
        'table' 	=> 'gr_users',
        'primary' 	=> 'id',
        'name'		=> 'user_data',
        'fields' => array(
            'email' 		=> array('validate' => 'is_email', 'required' => true),
			'password' 		=> array('validate' => 'is_string', 'required' => true),
			'access_token' 	=> array('validate' => 'is_string', 'required' => true),
			'date_upd'		=> array('validate' => 'is_string', 'required' => true),
			'date_add'		=> array('validate' => 'is_string', 'required' => true),
        ),
    );

	/**
	 * @return array
     */
	public function attributeLabels()
    {
    	return array(
    		'email' 	=>  __('Email', 'gr'),
    		'password' 	=>  __('Password', 'gr'),
    	);
    }

	/**
	 * @return array
     */
	public function rules()
    {
		$label = $this->attributeLabels();
    	return array(
    		'form'	=> array(
    			'action' 			=> '',
    			'method'			=> 'POST',
    			'class'				=> 'form',
    		),
			'input'	=> array(
				'email' 	=>  array(
					'before'        => '<div class="form-group row"><label for="email" class="col-lg-3">'.$label['email'].'*</label>',
					'after'         => '</div>',
					'type' 			=> 'text',
					'class'         => 'col-lg-9',
					'id'            => 'email',
				),
				'password' 	=>  array(
					'before'        => '<div class="form-group row"><label for="pass" class="col-lg-3">'.$label['password'].'*</label>',
					'after'         => '</div>',
					'type' 			=> 'text',
					'class'         => 'col-lg-9',
					'id'            => 'pass',
				),
			),
	    	'submit' => array(
	    		'before'			=> '<div class="row">',
	    		'after'				=> '<a class="col-lg-5" href="#">'.__('Forgot password?').'</a><div class="sign col-lg-12"><a  href="#">'. __('Registration').'</a></div>',
	    		'class' 			=> 'btn col-lg-4 col-lg-offset-3',
				'value'				=> __('Login')
	    	),
    	);
    }

	/**
	 * @return bool
	 * @throws Exception
     */
	public function add()
	{
		parent::__construct();

		/** check email */
		$email = $this->wpdb->get_var("SELECT `email` FROM $this->table WHERE `email` = '$this->email'");
		if($email || !empty($email))
			$this->message_errors = __('This email already exists');

		if (!empty($this->message_errors))
			return false;

		$this->password = wp_hash_password($this->password);
		$this->access_token = wp_hash_password(rand(rand(10, 1000), rand(20, 2000)));
		return parent::add();
	}

	/**
	 * @return bool
	 * @throws Exception
     */
	public function login()
	{
		parent::__construct();

		$user = $this->wpdb->get_row("
				SELECT `id`, `email`, `password` 
				FROM $this->table 
				WHERE `email` = '$this->email'
		");

		if (!$user || empty($user)) {
			$this->message_errors = __('Incorrect email or password');
			return false;
		}

		require_once( ABSPATH . WPINC . '/class-phpass.php');
		$hasher = new PasswordHash(8, TRUE);

		if (!$hasher->CheckPassword($this->password, $user->password)){
			$this->message_errors = __('Incorrect email or password');
			return false;
		}

		setcookie('gr_id_user', $user->id, time()+60*60*24*30, '/');
		setcookie('gr_id_password', $user->password, time()+60*60*24*30, '/');

		if (!session_id())
			session_start();

		$_SESSION['gr_id_user'] = $user->id;
		$_SESSION['gr_id_password'] = $user->password;

		return true;
	}

	/**
	 * @return bool
     */
	public function isLogin()
	{
		if (!session_id())
			session_start();

		if (isset($_SESSION['gr_id_user']) && isset($_SESSION['gr_id_password'])) {
			$id = $_SESSION['gr_id_user'];
			$password = $_SESSION['gr_id_password'];
		}
		elseif (isset($_COOKIE['gr_id_user']) && isset($_COOKIE['gr_id_password'])) {
			$id = $_COOKIE['gr_id_user'];
			$password = $_COOKIE['gr_id_password'];
		}

		if (!isset($id) || !isset($password))
			return false;

		$user = $this->wpdb->get_row("
				SELECT `id`, `password` 
				FROM $this->table 
				WHERE `id` = '$id'
		");

		if (!$user || empty($user) || !isset($user))
			return false;

		if ($password != $user->password)
			return false;

		return true;
	}

	/**
	 * @return array|bool
     */
	public function getUsers()
	{
		parent::__construct();

		$users = $this->wpdb->get_results("SELECT * FROM $this->table", ARRAY_A);

		return $this->transformToObjectClass($users);
	}
}

// $user = new User();
// var_dump($user->getUsers());