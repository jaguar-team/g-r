<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 14.11.2016 14:03
 * Version: 1.0.0
 */

function crop_string($string, $count, $add_symbol = '') {
    return substr($string, 0, $count).((strlen($string) > $count) ? $add_symbol : '');
}