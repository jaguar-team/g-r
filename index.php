<?php 
/**
 * Template Name: Home
 * @package WordPress
 * @subpackage g-r
 */
get_header(); ?>

<section class="top-slider">
	<?php get_template_part('part/home/slider'); ?>
</section>
<section class="about">	
	<div class="container">
		<div class="content">
			<div class="title">
				О компании
			</div>
			<div class="body">
				<?php _e('Инвестицинная компания — организация, осуществляющая коллективные инвестиции. Главными функциями инвестиционных компаний являются диверсификация инвестиций и управление инвестиционным портфелем, в который входят ценные бумаги разных эмитентов и другие виды');?>				
			</div>
			<button class="btn"><?php _e('Подробнее');?></button>
		</div>
	</div>	
</section>
<section class="advantage">
	<div class="container">
		<?php get_template_part('part/home/advantage'); ?>
	</div>
</section>
<section class="investment">
	<div class="container">
		<?php get_template_part('part/home/investing-package'); ?>
	</div>
</section>
<section class="questions">
	<div class="container">
		<?php get_template_part('part/home/ask-questions'); ?>
	</div>
</section>

<?php get_footer(); ?>