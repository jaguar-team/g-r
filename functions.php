<?php
/**
 * Author: Vlad Dneprov, https://www.linkedin.com/in/vladislav-dneprov
 * Author URI: http://jaguar-team.com
 * Date: 11.11.2016
 * Version: 1.0.0
 */

load_theme_textdomain( 'gr', get_template_directory() . '/languages' );

require_once ( __DIR__.'/core/custom-post.php' );
require_once ( __DIR__.'/core/breadcrumbs.php' );
require_once ( __DIR__.'/core/settings-themes.php' );
require_once ( __DIR__.'/core/user-functions.php' );

require_once ( __DIR__.'/core/install-db.php' );
require_once ( __DIR__.'/core/model/ObjectModel.php' );
require_once ( __DIR__.'/core/model/User.php' );
require_once ( __DIR__.'/core/model/Bid.php' );

global $user;
global $bid;
global $is_login;
$user = new User();
$bid = new Bid();
$is_login = $user->isLogin();

/** add javascript **/
add_action( 'wp_enqueue_scripts', 'add_scripts' );
function add_scripts() {
	
	/** lib **/
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap', 		get_template_directory_uri().'/js/lib/bootstrap.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'slick', 			get_template_directory_uri().'/js/lib/slick/slick.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'masked', 			get_template_directory_uri().'/js/lib/jquery.inputmask.bundle.min.js', array( 'jquery' ), '', true );

	/** custom **/
	wp_enqueue_script( 'main', 				get_template_directory_uri().'/js/main.js', array( 'jquery' ), '', true );

}

/** add css **/
add_action( 'wp_print_styles', 'add_styles' );
function add_styles() {

	/** lib **/
	wp_enqueue_style( 'bootstrap', 			get_template_directory_uri().'/css/lib/bootstrap.min.css' );
	wp_enqueue_style( 'slick', 				get_template_directory_uri().'/js/lib/slick/slick.css' );
	wp_enqueue_style( 'slick-theme', 		get_template_directory_uri().'/js/lib/slick/slick-theme.css' );

	/** fonts **/
	wp_enqueue_style( 'font-awesome', 		get_template_directory_uri().'/css/lib/font-awesome.min.css' );

	/** custom **/
	wp_enqueue_style( 'main', 				get_template_directory_uri().'/style.css' );
	wp_enqueue_style( 'global', 			get_template_directory_uri().'/css/global.css' );
    wp_enqueue_style( 'global-tablet', 		get_template_directory_uri().'/css/global-tablet.css' );
    wp_enqueue_style( 'global-mobile', 		get_template_directory_uri().'/css/global-mobile.css' );

	if ( is_home() ) {
        wp_enqueue_style( 'index', 			get_template_directory_uri().'/css/index.css' );
        wp_enqueue_style( 'index-tablet', 	get_template_directory_uri().'/css/index-tablet.css' );
        wp_enqueue_style( 'index-mobile', 	get_template_directory_uri().'/css/index-mobile.css' );
    }

	if ( is_single() ) {
		wp_enqueue_style( 'single', 		get_template_directory_uri().'/css/single.css' );
        wp_enqueue_style( 'single-tablet', 	get_template_directory_uri().'/css/single.css' );
        wp_enqueue_style( 'single-mobile', 	get_template_directory_uri().'/css/single.css' );
	}

	if ( is_page_template( 'tpl/contacts.php' ) ) {
        wp_enqueue_style ( 'tpl-contacts',	        get_template_directory_uri().'/css/tpl/contacts.css' );
        wp_enqueue_style ( 'tpl-contacts-tablet',	get_template_directory_uri().'/css/tpl/contacts.css' );
        wp_enqueue_style ( 'tpl-contacts-mobile',	get_template_directory_uri().'/css/tpl/contacts.css' );
    }
    if ( is_page_template( 'tpl/questions.php' ) ) {
        wp_enqueue_style( 'index',                   get_template_directory_uri().'/css/index.css' );
        wp_enqueue_style ( 'tpl-questions',          get_template_directory_uri().'/css/tpl/contacts.css' );
        wp_enqueue_style ( 'tpl-questions-tablet',   get_template_directory_uri().'/css/tpl/contacts.css' );
        wp_enqueue_style ( 'tpl-questions-mobile',   get_template_directory_uri().'/css/tpl/contacts.css' );
    }
}

add_theme_support( 'post-thumbnails' ); # add thubnails

remove_filter( 'the_content', 'wpautop' );

/** registration nav-menu **/
register_nav_menus(array(
	'top' 			=> __( 'Main menu', 'gr' ),
	'footer_1' 		=> __( 'Footer, column 1', 'gr' ),
    'footer_2' 		=> __( 'Footer, column 2', 'gr' ),
    'footer_3' 		=> __( 'Footer, column 3', 'gr' ),
    'languages'     => __( 'Languages' )
));

function remove_menus(){
    global $menu;
    $restricted = array(
        __('Posts'),
        __('Links'),
        __('Appearance'),
        __('Comments'),
    );
    end ($menu);
    while (prev($menu)){
        $value = explode(' ', $menu[key($menu)][0]);
        if( in_array( ($value[0] != NULL ? $value[0] : "") , $restricted ) ){
            unset($menu[key($menu)]);
        }
    }
}
add_action('admin_menu', 'remove_menus');

function change_login_logo() {
    echo '
    <style type="text/css">
        .login h1 a {
            background: url('.get_template_directory_uri().'/img/logo.png) no-repeat 0 0 !important;
            height: 100px;
            margin: 0 auto;
        }
        .login h1 {
            background-color: #000f2c;
            padding: 15px 0;
        }
        .login form { margin: 0; }
    </style>';
}

add_action( 'login_head', 'change_login_logo' );
add_filter( 'login_headerurl', create_function('', 'return get_home_url();') );
add_filter( 'login_headertitle', create_function('', 'return "GR";') );

add_action('add_admin_bar_menus', 'reset_admin_wplogo');
function reset_admin_wplogo(  ){
    remove_action( 'admin_bar_menu', 'wp_admin_bar_wp_menu', 10 );

    add_action( 'admin_bar_menu', 'my_admin_bar_wp_menu', 10 );
}
function my_admin_bar_wp_menu( $wp_admin_bar ) {
    $wp_admin_bar->add_menu( array(
        'id'    => 'wp-logo',
        'title' => '<img style="width:20px;" src="'.get_template_directory_uri().'/img/logo.png" />',
        'href'  => get_home_url(),
        'meta'  => array(
            'title'     => 'Перейти на сайт',
            'target'    => 'blank'
        ),
    ) );
}