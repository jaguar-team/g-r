<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 11.11.2016 13:35
 * Version: 1.0.0
 * Template Name: Contacts
 */
__( 'Contacts' , 'gr' );
get_header();
?>

<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php get_template_part('part/breadcrumbs'); ?>
			</div>
			<div class="title col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center-sm">
				<?php _e('Контакты', 'gr'); ?>
			</div>
			<div class="support col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center-sm">
				<?php _e('Служба клентской поддержки', 'gr');?>
				<ul>
					<li>
						<a href="tel:+38(057)666888"><span><img src="<?= get_template_directory_uri().'/img/phone-grey.png'; ?>">+38 (057) 666 888</span></a>
					</li>
					<li>
						<a href=""><span><img src="<?= get_template_directory_uri().'/img/env-grey.png'; ?>">gr-investing@gmail.com</span></a>
					</li>
					<li>
						<span><img src="<?= get_template_directory_uri().'/img/skype-grey.png'; ?>">investing</span>
					</li>
				</ul>
			</div>
			<div class="contact col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center-sm">
				<?php _e('Связаться с руководством Компании');?>
				<form class="text-center-sm"> 
					<textarea placeholder="<?php _e('Ваш вопрос', 'gr');?>"></textarea>
					<input type="" name="" placeholder="<?php _e('Ваш email', 'gr');?>">
					<input type="submit" name="">
				</form>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>

