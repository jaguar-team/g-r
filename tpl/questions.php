<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 11.11.2016 13:35
 * Version: 1.0.0
 * Template Name: Questions
 */
__( 'Questions' , 'gr' );
get_header();
?>

<section class="questions">
	<div class="container">
		<?php get_template_part('part/home/ask-questions'); ?>
	</div>
</section>
<section class="your-question">
	<div class="contact col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center-sm">
		<?php _e('Задайте свой вопрос', 'gr');?>
		<form class="text-center-sm"> 
			<textarea placeholder="<?php _e('Ваш вопрос', 'gr');?>"></textarea>
			<input type="" name="" placeholder="<?php _e('Ваш email', 'gr');?>">
			<input type="submit" name="">
		</form>
	</div>
</section>

<?php get_footer(); ?>

