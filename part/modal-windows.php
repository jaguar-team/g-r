<?php

global $user;
global $bid;
global $is_login;

?>

<!--modals-->
<div class="modal fade" id="modal-investment" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php _e('Invest', 'gr');?></h4>
            </div>
            <div class="modal-body">
                <?= $bid->getForm(); ?>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php  if (!$is_login): ?>
    <div class="modal fade" id="modal-login" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Enter to your account', 'gr');?></h4>
                </div>
                <div class="modal-body">
                    <?= $user->getForm(); ?>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php endif; ?>

<div class="modal fade" id="modal-forget" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php _e('Forgot your password?', 'gr');?></h4>
                <span><?php _e('Введите ваш email для получения нового пароля');?></span>
            </div>
            <div class="modal-body">
                <form class="form">
                    <div class="form-group row">
                        <label for="email" class="col-lg-3"><?php _e('Email');?>*</label>
                        <input id="email" class="col-lg-9" type="text" name="email">
                    </div>
                    <div class="row">
                        <input class="col-lg-9 col-lg-offset-3" type="submit" name="">
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modal-success" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php _e('Ссылка на смену
							пароля отправлена
							на ваш email');?></h4>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /.modals -->