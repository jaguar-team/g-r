<?php
/**
 * Author: Vladislav Dneprov
 * Author URI: https://www.linkedin.com/in/vladislav-dneprov
 * Date: 16.11.2016 12:48
 * Version: 1.0.0
 */

$args = array(
    'numberposts'     => 5,
    'offset'          => 0,
    'category'        => '',
    'orderby'         => 'post_date',
    'order'           => 'ASC',
    'include'         => '',
    'exclude'         => '',
    'meta_key'        => '',
    'meta_value'      => '',
    'post_type'       => 'slider',
    'post_status'     => 'publish'
);

$slides = get_posts($args);
?>

<?php if ( $slides ):?>
    <?php foreach ( $slides as $slide ): ?>
        <div class="item">
            <?= get_the_post_thumbnail( $slide->ID, 'full' ); ?>
            <div class="content">
                <div class="container">
                    <div class="desc">
                        <div class="title">
                            <?= $slide->post_title; ?>
                        </div>
                        <div class="text">
                            <?= $slide->post_content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
