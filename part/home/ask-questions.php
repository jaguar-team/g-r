<?php

$args = array(
    'numberposts'     => 3,
    'offset'          => 0,
    'category'        => '',
    'orderby'         => 'post_date',
    'order'           => 'ASC',
    'include'         => '',
    'exclude'         => '',
    'meta_key'        => '',
    'meta_value'      => '',
    'post_type'       => 'questions',
    'post_status'     => 'publish'
);

$questions = get_posts($args);

?>
<div class="content">
	<div class="row">
		<div class="title">
			<?php _e( 'Most often questions' ); ?>
		</div>
		<ul>
            <?php if ( $questions ):?>
                <?php foreach ( $questions as $number => $question ): ?>
                    <li>
                        <span><i class="fa fa-angle-down"></i><a href="#q<?= $number + 1; ?>"  data-toggle="collapse"><?= $question->post_title; ?></a></span>
                        <div id="q<?= $number + 1; ?>" class="collapse"><?= $question->post_content; ?></div>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
		</ul>
		<div class="text-center">
			<a class="btn"><?php _e( 'I still have some questions', 'gr' ); ?></a>
		</div>
	</div>
</div>