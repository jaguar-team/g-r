<?php

$args = array(
    'numberposts'     => 6,
    'offset'          => 0,
    'category'        => '',
    'orderby'         => 'post_date',
    'order'           => 'ASC',
    'include'         => '',
    'exclude'         => '',
    'meta_key'        => '',
    'meta_value'      => '',
    'post_type'       => 'investment-company',
    'post_status'     => 'publish'
);

$investing_packages = get_posts($args);

?>
<div class="row">
	<div class="title col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php _e( 'Investing packages', 'gr' ); ?>
	</div>
    <?php if ( $investing_packages ):?>
        <?php foreach ($investing_packages as $package): ?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="block <?= ( get_post_meta( $package->ID, 'vip', 1 )  ? 'vip' : '' ); ?>">
                    <div class="content">
                        <div class="content-header">
                            <?= $package->post_title; ?>
                        </div>
                        <div class="content-body">
                            <h2>
                                <?php if ( get_post_meta( $package->ID, 'vip', 1 ) ): ?>
                                    <?= get_post_meta( $package->ID, 'amount-from', 1 ); ?> - ... $
                                <?php else: ?>
                                    <?= get_post_meta( $package->ID, 'amount-from', 1 ); ?> - <?= get_post_meta( $package->ID, 'amount-to', 1 ); ?> $
                                <?php endif; ?>
                            </h2>
                            <span><?php _e( 'Interest rate', 'gr' ); ?></span>
                            <span class="title"><?= get_post_meta( $package->ID, 'interest-rate', 1 ); ?>%</span>
                            <span><?php _e( 'Period of investment', 'gr' ); ?></span>
                            <span class="time">
                                <?php if ( get_post_meta( $package->ID, 'vip', 1 ) ): ?>
                                    <?= _e( 'All conditions are discussing individually', 'gr' ); ?>
                                <?php else: ?>
                                    <?= get_post_meta( $package->ID, 'interest-period', 1 ).' '.__( 'months', 'gr' ); ?>
                                <?php endif; ?>
                            </span>
                        </div>
                        <div class="content-footer">
                            <?php if ( get_post_meta( $package->ID, 'vip', 1 ) ): ?>
                                <?= ''; ?>
                            <?php else: ?>
                                12 <?php _e( 'months', 'gr' ); ?> + 1%
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <?php else: ?>
        <?php _e( 'Investing packages are not found', 'gr' ); ?>
    <?php endif; ?>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
		<a class="btn"><?php _e( 'More info', 'gr' ); ?></a>
	</div>
</div>