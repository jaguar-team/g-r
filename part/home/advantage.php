<?php
/**
 * Created by PhpStorm.
 * User: Vlad.Dneprov
 * Date: 11.11.2016
 * Time: 13:14
 */

$args = array(
	'numberposts'     => 6,
	'offset'          => 0,
	'category'        => '',
	'orderby'         => 'post_date',
	'order'           => 'ASC',
	'include'         => '',
	'exclude'         => '',
	'meta_key'        => '',
	'meta_value'      => '',
	'post_type'       => 'advantages',
	'post_status'     => 'publish'
);

$advantages = get_posts( $args );

?>
<div class="row">
	<div class="title col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php _e( 'Advantages', 'gr' ); ?>
	</div>

    <?php if ( $advantages ): ?>
        <?php foreach ( $advantages as $number => $advantage ): ?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="block">
                    <?= get_the_post_thumbnail( $advantage->ID, 'full' ); ?>
                    <div class="content">
                        <div class="desc short">
                            <?= $advantage->post_title; ?>
                        </div>
                        <div class="desc full">
                            <?= $advantage->post_content; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <?php _e( 'Advantages are not found', 'gr' ); ?>
    <?php endif; ?>
	
</div>